package ist.challenge.adri_hilda.rest.api;

import java.util.List;

import org.apache.coyote.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.adri_hilda.model.Users;
import ist.challenge.adri_hilda.service.user.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("api/users")
public class UserController {

    private UserService userService;

    // build create User REST API
    @PostMapping
    public ResponseEntity<Users> createUser(@RequestBody Users user){
        Users savedUser = userService.createUser(user);
        return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
        
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody Users user) throws BadRequestException {
        if(user.getUsername() == null || user.getPassword() == null){
            throw new BadRequestException("Username dan / atau password kosong");
        }
        if(userService.login(user.getUsername(),user.getPassword())){
            return new ResponseEntity<>("Sukses Login", HttpStatus.OK);
        }else{
            log.info("Masuk sini");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    // build get user by id REST API
    // http://localhost:8080/api/users/1
    @GetMapping("{id}")
    public ResponseEntity<Users> getUserById(@PathVariable("id") Long userId){
        Users user = userService.getUserById(userId);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    // Build Get All Users REST API
    // http://localhost:8080/api/users
    @GetMapping
    public ResponseEntity<List<Users>> getAllUsers(){
        List<Users> users = userService.getAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    // Build Update User REST API
    @PutMapping("{id}")
    // http://localhost:8080/api/users/1
    public ResponseEntity<Users> updateUser(@PathVariable("id") Long userId,
                                           @RequestBody Users user){
        user.setId(userId);
        Users updatedUser = userService.updateUser(user);
        return new ResponseEntity<>(updatedUser, HttpStatus.CREATED);
    }

    // Build Delete User REST API
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Long userId){
        userService.deleteUser(userId);
        return new ResponseEntity<>("User berhasil dihapus", HttpStatus.OK);
    }
}
