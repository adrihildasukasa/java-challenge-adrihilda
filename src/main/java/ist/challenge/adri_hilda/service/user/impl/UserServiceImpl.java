package ist.challenge.adri_hilda.service.user.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import ist.challenge.adri_hilda.config.exception.BadRequestException;
import ist.challenge.adri_hilda.config.exception.ConflictException;
import ist.challenge.adri_hilda.config.exception.UnauthorizedException;
import ist.challenge.adri_hilda.model.Users;
import ist.challenge.adri_hilda.repository.UsersRepository;
import ist.challenge.adri_hilda.service.user.UserService;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private UsersRepository userRepository;

    public Users createUser(Users user) {
        Optional<Users> checkUser = checkUser(user.getUsername());
        if(checkUser.isPresent())
            throw new ConflictException("Username '" + user.getUsername() + "' sudah ada.");
        return userRepository.save(user);
    }

    @Override
    public Users getUserById(Long userId) {
        Optional<Users> optionalUser = userRepository.findById(userId);
        return optionalUser.get();
    }

    @Override
    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Users updateUser(Users user) {
        Users existingUser;
        if(user.getUsername()!=null){
            Optional<Users> checkUser = checkUser(user.getUsername());
            if(checkUser.isPresent())
                throw new ConflictException("Username '" + user.getUsername() + "' sudah ada.");
            existingUser = checkUser.get();
            existingUser.setUsername(user.getUsername());
        }else{
            existingUser = userRepository.findById(user.getId()).get();
        }
        if(user.getPassword()!=null){
            if(user.getPassword().equals(existingUser.getPassword()))
                throw new BadRequestException("Password tidak boleh sama dengan password sebelumnya");
            else
                existingUser.setPassword(user.getPassword());
        }
        Users updatedUser = userRepository.save(existingUser);
        return updatedUser;
    }

    @Override
    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public Optional<Users> checkUser(String username) {
       return userRepository.findByUsername(username);
    }

    @Override
    public boolean login(String username, String passwword) {
        Optional<Users> checkUser = checkUser(username);
        if(checkUser.isPresent()){
            Users users = checkUser.get();
            if(!users.getPassword().equals(passwword))
                throw new UnauthorizedException("");
            else
                return true;
        }else
            return false;
    }
    
}
