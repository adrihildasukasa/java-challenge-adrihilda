package ist.challenge.adri_hilda.service.user;

import java.util.List;
import java.util.Optional;

import ist.challenge.adri_hilda.model.Users;

public interface UserService {
    
    Users createUser(Users user);

    Users getUserById(Long userId);

    Optional<Users> checkUser(String username);

    boolean login(String username,String passwword);

    List<Users> getAllUsers();

    Users updateUser(Users user);

    void deleteUser(Long userId);
}
