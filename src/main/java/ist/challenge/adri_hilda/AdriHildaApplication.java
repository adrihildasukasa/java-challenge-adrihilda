package ist.challenge.adri_hilda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdriHildaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdriHildaApplication.class, args);
	}

}
