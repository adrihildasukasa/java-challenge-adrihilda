package ist.challenge.adri_hilda.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import ist.challenge.adri_hilda.model.Users;

public interface UsersRepository extends JpaRepository<Users, Long> {

    Optional<Users> findByUsername(String username);
    Optional<Users> findByUsernameAndPassword(String username,String passsword);

}
